function tplawesome(e,t){res=e;for(var n=0;n<t.length;n++){res=res.replace(/\{\{(.*?)\}\}/g,function(e,r){return t[n][r]})}return res}

$(function() {
    $("form").on("submit", function(e) {
        e.preventDefault();
        // Prepare request.
        var request = gapi.client.youtube.videos.list({
            part: "snippet",
            type: "video",
            maxResults: 5,
            regionCode: "NL",
            chart: "mostPopular",
            videoCategoryId: ""
        });
        // Execute the request.
        request.execute(function(response) {
            var results = response.result;
            $.each(results.items, function(index, item) {
                $.get("tpl/item.html", function(data) {
                    $("#results").append(tplawesome(data, [{"title":item.snippet.title, "videoid":item.id}]));
                });
            });
        });
    });
});

function init() {
     gapi.client.setApiKey("AIzaSyDg-4yWVr5pZ0vMA3XPBhhzpnTMsrg4NWI");
     gapi.client.load("youtube", "v3", function() {
         // Youtube API loaded.
     });
};
